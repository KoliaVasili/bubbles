import pygame
from os import environ
from sys import platform as _sys_platform
from random import randint, choice
from math import dist as math_dist

# objects

def keyboard_state(state):

    if state:
        pygame.key.start_text_input()
    else:
        pygame.key.stop_text_input()

def platform():
    if 'ANDROID_ARGUMENT' in environ:
        return 'android'
    elif _sys_platform in ('linux', 'linux2', 'linux3'):
        return 'linux'
    elif _sys_platform in ('win32', 'cygwin'):
        return 'windows'

class Path:
    def __new__(cls, path):
        plat = platform()
        if plat == 'android':
            return '/data/data/org.test.cirgame/files/app/'+path
        elif plat == 'linux':
            return './'+path
        elif plat == 'windows':
            return path

class Button:
    def __init__(self, pos, size, colour, text):
        self.pos = pos # center
        self.size = size
        self.colour = colour
        self.text = text
        self.create_button()

    def create_button(self):

        self.image = pygame.Surface(self.size, pygame.SRCALPHA)
        pygame.draw.rect(self.image, self.colour, (0, 0, self.size[0], self.size[1]), 3*2)
        self.rect = self.image.get_rect(center=self.pos)
        text_surf = font.render(self.text, True, self.colour)
        text_rect = text_surf.get_rect(center=(self.size[0]//2, self.size[1]//2))
        self.image.blit(text_surf, text_rect)

class Circle:
    def __init__(self, pos, radius, colour):
        self.pos = pos
        self.radius = radius
        self.colour = colour

class Main:

    def __init__(self):
        self.amount = 20
        self.gaming = False
        self.circles = []
        self.init_time = 0
        self.total_time = 0
        self.keyboard = False
        self.str = ''
        # self.generate_circles() # runs in the start button
        self.start_button = Button((W//2, H//2), (200*2, 100*2), 'grey28', 'Start')
        self.exit_button = Button((470*2, 30*2), (30*2, 30*2), 'grey28', 'x')
        self.keyboard_button = Button((30*2, 30*2), (30*2, 30*2), 'grey28', 'o')

    def generate_circles(self):
        self.circles = []
        for _ in range(self.amount):
            pos = randint(20*2, 460*2), randint(100*2, 700*2)
            radius = randint(18*2, 60*2)
            colour = choice(COLORS)
            self.circles.append(Circle(pos, radius, colour))
    
    def run_game(self, MOUSE):

        total_time = pygame.time.get_ticks() - self.init_time
        time_surf = font_num.render(f'{total_time//1000}.{total_time%1000}', True, 'grey28')
        time_rect = time_surf.get_rect(center=(W//2, 30*2))
        screen.blit(time_surf, time_rect)

        for index, circle in enumerate(self.circles[::-1]):
            pygame.draw.circle(screen, circle.colour, circle.pos, circle.radius)
            if MOUSE and math_dist(MOUSE, circle.pos) <= circle.radius:
                del_index = index
        try:
            self.circles.pop(len(self.circles)-del_index-1)
            click_sound.play()
        except UnboundLocalError: pass

        if not self.circles:
            win_sound.play()
            self.total_time = pygame.time.get_ticks()-self.init_time
            self.gaming = False

    def run_pre_game(self, MOUSE):
        screen.blit(self.start_button.image, self.start_button.rect)
        if MOUSE and self.start_button.rect.collidepoint(MOUSE):
            self.gaming = True
            self.init_time = pygame.time.get_ticks()
            self.generate_circles()
            click_sound.play()
        if self.total_time:
            time_surf = font_num.render(f'Total Time: {self.total_time//1000}.{self.total_time%1000}sec', True, 'grey28')
            time_rect = time_surf.get_rect(center=(W//2, 305*2))
            screen.blit(time_surf, time_rect)

    def run(self, EVENT):

        MOUSE = False
        for event in EVENT:
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                MOUSE = event.pos
            if event.type == pygame.KEYDOWN and event.key == pygame.K_BACKSPACE:
                    self.str = self.str[:-1]
            elif event.type == pygame.TEXTINPUT:
                    self.str += event.text

        if MOUSE :
            if self.exit_button.rect.collidepoint(MOUSE):
                pygame.quit()
                raise SystemExit
            if self.keyboard_button.rect.collidepoint(MOUSE):
                click_sound.play()
                self.keyboard = not self.keyboard
                keyboard_state(self.keyboard)

        screen.fill('grey77')
        screen.blit(self.exit_button.image, self.exit_button.rect)
        screen.blit(self.keyboard_button.image, self.keyboard_button.rect)
        text_surf = font_num.render(self.str, True, 'grey28')
        text_rect = text_surf.get_rect(midtop=(W//2, 30*2))
        screen.blit(text_surf, text_rect)

        if self.gaming:
            self.run_game(MOUSE)
        else:
            self.run_pre_game(MOUSE)

# initializing

W, H = 500*2, 830*2

pygame.init()
screen = pygame.display.set_mode((W, H), pygame.FULLSCREEN | pygame.SCALED)
pygame.display.set_caption('Click Circles')
clock = pygame.time.Clock()
font = pygame.font.Font(Path('fonts/KidzOnlyTooSSKBold.ttf'), 35*2)
font_num = pygame.font.Font(Path('fonts/RobotoCondensed-Bold.ttf'), 35*2)

# sound

click_sound = pygame.mixer.Sound(Path('sounds/click.wav'))
win_sound = pygame.mixer.Sound(Path('sounds/win.wav'))

# vars
FPS = 60
_COLORS = list(pygame.colordict.THECOLORS.keys())
COLORS = [c for c in _COLORS if 'grey' not in c and 'gray' not in c]
main = Main()

# game loop
while True:

    pygame.display.update()
    clock.tick(FPS)
    EVENT = pygame.event.get().copy()

    for event in EVENT:
        if event.type == pygame.QUIT:
            pygame.quit()
            raise SystemExit

    main.run(EVENT)
